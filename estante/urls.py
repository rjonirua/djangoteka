from django.conf.urls import patterns, include, url
from .views import LinkList, LivroList, contar_cliques, contatar_me, sobre, \
   email_me, email_enviado

urlpatterns = patterns('',
   url(r'^$', LinkList.as_view(mtipo='TAPP'), name='links-tutoriais'),
   url(r'^links/(?P<obj_id>[0-9]+)/$', contar_cliques, name='contar-cliques-link'),
   url(r'^github/$', LinkList.as_view(mtipo='GHUB'), name='links-github'),
   #url(r'^livros/$', LivroList.as_view(), name='livros' ),
   url(r'^livros/django/$', LivroList.as_view(mtipo='DJA'), name='livros-django' ),
   url(r'^livros/django/(?P<obj_id>[0-9]+)/$', contar_cliques, name='contar-cliques-livros-django' ),
   #url(r'contato/$', contatar_me, name='contatar-me'),
   url(r'contato/$', email_me, name='contatar-me'),
   url(r'sobre/$', sobre, name='sobre'),
   url(r'email_enviado/$', email_enviado, name='email-enviado')
)