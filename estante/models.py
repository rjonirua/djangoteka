# coding: utf-8
from django.db import models
from django.utils import timezone
#import hashlib


class Tag(models.Model):
   nome = models.CharField(max_length=100, unique=True)

   class Meta:
      ordering = ['nome']

   def __unicode__(self):
      return self.nome


class Link(models.Model):
   ARTIGO = 'ART'
   TUTORIAL_APP = 'TAPP'
   GITHUB = 'GHUB'
   CHOICES_LINK_TIPOS = (
      (ARTIGO, 'artigo'),
      (TUTORIAL_APP, 'tutorial app'),
      (GITHUB, 'github'),
   )
   titulo = models.CharField(max_length=500)
   url = models.URLField(max_length=255, unique=True)
   descricao = models.TextField()
   tags = models.ManyToManyField(Tag, related_name='links')
   tipo = models.CharField(max_length=6, choices=CHOICES_LINK_TIPOS,
            default=TUTORIAL_APP)
   dt_post_origem = models.DateField(blank=True, null=True) 
   dt_checagem = models.DateTimeField() 
   cliques = models.PositiveIntegerField(default=0)
   dt_criacao = models.DateTimeField(default=timezone.now)

   # --- doc ---
   # url_md5: usado para guardar a md5sum da url. alternativa para 'burlar' o 
   #   limite de 255 caracteres do mysql para campos de texto marcados como 
   #   'unique' (caso o campo url fosse marcado como unique)
   # dt_post_origem: data da publicação original do link (no blog original)
   # dt_chegagem: última vez que o link foi confirmado com ativo

   '''
   class Meta:
      ordering = ['titulo']
   '''

   def __unicode__(self):
      return self.titulo

   def get_tags(self):
      return ', '.join([c.nome for c in self.tags.all()])

'''
   def get_url_md5(self):
      h = hashlib.md5()
      h.update(self.url.encode('utf-8'))
      return h.hexdigest()
'''

class Livro(models.Model):
   DJANGO = 'DJA'
   PYTHON = 'PYT'
   OUTROS = 'OUT'
   CHOICES_LIVRO_TIPOS = (
      (DJANGO, 'django'),
      (PYTHON, 'python'),
      (OUTROS, 'outros'),
   )
   titulo = models.CharField(max_length=300)
   url = models.URLField(max_length=255, unique=True)
   descricao = models.TextField()
   tags = models.ManyToManyField(Tag, related_name='livros')
   tipo = models.CharField(max_length=3,choices=CHOICES_LIVRO_TIPOS,
            default=DJANGO)
   gratis = models.BooleanField(default=False)
   capa = models.ImageField(upload_to="capas", default='capas/none.jpg')
   ano_edicao = models.PositiveIntegerField(blank=True, null=True)
   dt_checagem = models.DateTimeField()
   cliques = models.PositiveIntegerField(default=0)
   dt_criacao = models.DateTimeField(default=timezone.now)

   class Meta:
      ordering = ['titulo']

   def __unicode__(self):
      return self.titulo

   def get_tags(self):
      return ", ".join([t.nome for t in self.tags.all()])