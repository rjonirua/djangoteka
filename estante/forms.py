from django import forms


class ContatoForm(forms.Form):
	nome = forms.CharField(label='Seu nome', max_length=100,
		widget=forms.TextInput(attrs={'class': "form-control", 'placeholder':'Seu nome *'}))
	email = forms.EmailField(required=False,
		widget=forms.EmailInput(attrs={'class': "form-control", 'placeholder':'Seu e-mail'}))
	site = forms.URLField(required=False,
		widget=forms.URLInput(attrs={'class': "form-control", 'placeholder':'Seu site'}))
	assunto = forms.CharField(max_length=100,
		widget=forms.TextInput(attrs={'class': "form-control", 'placeholder':'Assunto *'}))
	mensagem = forms.CharField(
		widget=forms.Textarea(attrs={'class': "form-control", 'placeholder':'Mensagem *'}))