from django.contrib import admin
from .models import Tag, Link, Livro


class LinkAdmin(admin.ModelAdmin):
   list_display = ('titulo', 'descricao', 'url', 'get_tags', 'dt_post_origem', 
                     'cliques', 'tipo')
   list_filter = ('dt_post_origem', 'dt_checagem', 'tags', 'tipo')
   search_fields = ['titulo', 'url']

class LivroAdmin(admin.ModelAdmin):
   list_display = ('titulo', 'descricao', 'ano_edicao', 'url', 'get_tags', 
                     'dt_checagem', 'cliques', 'tipo')

class TagAdmin(admin.ModelAdmin):
   list_display = ('nome',)


admin.site.register(Tag, TagAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Livro, LivroAdmin)