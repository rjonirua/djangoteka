# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0007_auto_20150812_2147'),
    ]

    operations = [
        migrations.RenameField(
            model_name='link',
            old_name='tipos',
            new_name='tipo',
        ),
        migrations.RenameField(
            model_name='livro',
            old_name='tipos',
            new_name='tipo',
        ),
    ]
