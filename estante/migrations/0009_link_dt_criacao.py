# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0008_auto_20150814_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='dt_criacao',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
