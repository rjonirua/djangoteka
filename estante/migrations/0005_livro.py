# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0004_auto_20150812_1000'),
    ]

    operations = [
        migrations.CreateModel(
            name='Livro',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('titulo', models.CharField(max_length=300)),
                ('url', models.URLField(unique=True, max_length=255)),
                ('descricao', models.TextField()),
                ('tipo', models.CharField(default='DJA', choices=[('DJA', 'django'), ('PYT', 'python'), ('OUT', 'outros')], max_length=3)),
                ('gratis', models.BooleanField(default=True)),
                ('capa', models.ImageField(default='capas/none.jpg', upload_to='capas')),
                ('ano_edicao', models.PositiveIntegerField()),
                ('dt_checagem', models.DateTimeField()),
                ('cliques', models.PositiveIntegerField(default=0)),
                ('tags', models.ManyToManyField(to='estante.Tag', related_name='livros')),
            ],
            options={
                'ordering': ['titulo'],
            },
            bases=(models.Model,),
        ),
    ]
