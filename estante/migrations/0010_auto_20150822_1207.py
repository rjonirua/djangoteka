# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0009_link_dt_criacao'),
    ]

    operations = [
        migrations.AlterField(
            model_name='livro',
            name='gratis',
            field=models.BooleanField(),
        ),
    ]
