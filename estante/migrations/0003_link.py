# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0002_auto_20150811_2034'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=500)),
                ('url', models.CharField(max_length=500)),
                ('url_md5', models.CharField(max_length=255, unique=True)),
                ('descricao', models.TextField()),
                ('tipo', models.CharField(max_length=6, choices=[(('ART',), 'artigo'), (('TAPP',), 'tutorial app'), (('GHUB',), 'github')], default=('TAPP',))),
                ('dt_postagem', models.DateField(null=True, blank=True)),
                ('dt_checagem', models.DateTimeField()),
                ('cliques', models.PositiveIntegerField(default=0)),
                ('tags', models.ManyToManyField(to='estante.Tag', related_name='links')),
            ],
            options={
                'ordering': ['titulo'],
            },
            bases=(models.Model,),
        ),
    ]
