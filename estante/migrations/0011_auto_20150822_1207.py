# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0010_auto_20150822_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='livro',
            name='gratis',
            field=models.BooleanField(default=False),
        ),
    ]
