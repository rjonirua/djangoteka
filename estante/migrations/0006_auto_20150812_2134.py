# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0005_livro'),
    ]

    operations = [
        migrations.RenameField(
            model_name='link',
            old_name='tipo',
            new_name='tipos',
        ),
        migrations.RenameField(
            model_name='livro',
            old_name='tipo',
            new_name='tipos',
        ),
    ]
