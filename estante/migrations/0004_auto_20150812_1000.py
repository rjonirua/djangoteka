# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0003_link'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='link',
            name='url_md5',
        ),
        migrations.AlterField(
            model_name='link',
            name='tipo',
            field=models.CharField(max_length=6, choices=[('ART', 'artigo'), ('TAPP', 'tutorial app'), ('GHUB', 'github')], default='TAPP'),
        ),
        migrations.AlterField(
            model_name='link',
            name='url',
            field=models.URLField(max_length=255, unique=True),
        ),
    ]
