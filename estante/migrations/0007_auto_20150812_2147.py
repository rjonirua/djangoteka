# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0006_auto_20150812_2134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='livro',
            name='ano_edicao',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
