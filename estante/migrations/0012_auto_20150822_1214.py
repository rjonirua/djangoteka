# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estante', '0011_auto_20150822_1207'),
    ]

    operations = [
        migrations.RenameField(
            model_name='link',
            old_name='dt_postagem',
            new_name='dt_post_origem',
        ),
    ]
