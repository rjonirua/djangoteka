# coding: utf-8
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.views.generic import ListView
from .models import Tag, Link, Livro
from .forms import ContatoForm

def contar_cliques(request, obj_id):
   """ conta o nr de vezes em que um link é clicado """
   # TODO: melhorar?
   if request.path_info.startswith('/links/'):
      link = Link.objects.get(pk=obj_id)
      link.cliques += 1
      link.save()
      return HttpResponseRedirect(link.url)
   elif request.path_info.startswith('/livros/'):
      livro = Livro.objects.get(pk=obj_id)
      livro.cliques += 1
      livro.save()
      return HttpResponseRedirect(livro.url)


def get_tags_selecionadas(self):
   """ obtém as tags com checkboxs marcados """
   tags = Tag.objects.all()
   tags_selecionadas = []

   # TODO: ver a possibilidade de pegar o array de tags diretamente do request.
   #print('request.GET.items: ', self.request.GET)
   for tag in tags:
      tag_nome = 'cbox_' + tag.nome
      if self.request.GET.get(tag_nome):
         tags_selecionadas.append(tag)

   return tags_selecionadas


class LinkList(ListView):
   model = Link
   paginate_by = 5
   context_object_name = 'lista_de_links'
   mtipo = 'TAPP'

   def get_context_data(self, **kwargs):
      context = super(LinkList, self).get_context_data(**kwargs)
      context['tags_com_links'] = Tag.objects.filter(links__in=self.model.objects.filter(tipo=self.mtipo)).distinct()
      context['links_mais_visitados'] = self.model.objects.filter(tipo=self.mtipo).order_by('-cliques')[:5]
      context['links_mais_recentes'] = self.model.objects.filter(tipo=self.mtipo).order_by('-dt_criacao')[:5]
      context['tags_marcadas'] = get_tags_selecionadas(self)
      context['total_de_links'] = self.get_queryset().count()
      if self.mtipo == 'TAPP':
         context['titulo_pagina'] = 'Tutoriais - Aplicativos Completos'
      elif self.mtipo == 'GHUB':
         context['titulo_pagina'] = 'Códigos - Github, Bitbucket, etc'
      return context

   def get_queryset(self):
      links = self.model.objects.filter(tipo=self.mtipo)
      # recupera checkboxs marcados no filtro de tags
      tags_selecionadas = get_tags_selecionadas(self)

      if tags_selecionadas:
         return links.filter(tags__in=tags_selecionadas).distinct()
      else:
         return links


class LivroList(ListView):
   template_name = 'estante/livros_lista.html'
   model = Livro
   context_object_name = 'livros_lista'
   paginate_by = 9
   mtipo = 'DJA'

   def get_context_data(self, **kwargs):
      context = super(LivroList, self).get_context_data(**kwargs)
      context['tags_com_links'] = Tag.objects.filter(livros__in=self.model.objects.all()).distinct()
      context['links_mais_visitados'] = self.model.objects.all().order_by('-cliques')[:5]
      context['links_mais_recentes'] = self.model.objects.filter(tipo=self.mtipo).order_by('-dt_criacao')[:5]
      context['tags_marcadas'] = get_tags_selecionadas(self)
      context['total_de_livros'] = self.get_queryset().count()
      
      return context

   def get_queryset(self):
      livros = self.model.objects.filter(tipo=self.mtipo).order_by('-ano_edicao')
      tags_selecionadas = get_tags_selecionadas(self)

      if tags_selecionadas:
         return livros.filter(tags__in=tags_selecionadas).distinct()
      else:
         return livros


def contatar_me(request):
   email = 'alexandrejr.aurino@gmail.com'
   return render(request, 'estante/contato.html', {'email': email})

def sobre(request):
   return render(request, 'estante/sobre.html', {})
   
def email_me(request):
   if request.method == 'POST':
      form = ContatoForm(request.POST)
      if form.is_valid():
         # enviar email
         nome = form.cleaned_data['nome']
         email = form.cleaned_data['email']
         site = form.cleaned_data['site']
         assunto = form.cleaned_data['assunto']
         mensagem = form.cleaned_data['mensagem']
         msg = "De: " + nome + " (" + email+ ")\n"
         msg += "Site: " + site + "\n"
         msg += "\n"
         msg += mensagem
         assunto = "[Djangoteca] " + assunto
         destinatario = ['alexandrejr.aurino@gmail.com', email]
         send_mail(assunto, msg, email, destinatario)
         return HttpResponseRedirect('/email_enviado/', {
            'alerta': 'Enviado com sucesso!'})
   else:
      form = ContatoForm()
      
   return render(request, 'estante/contato.html', {'form': form})

def email_enviado(request):
   return render(request, 'estante/email_enviado.html', {})