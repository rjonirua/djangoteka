from django.conf.urls import patterns, include, url
from django.contrib import admin
# --- para trabalhar com imagens
from django.conf import settings
from django.conf.urls.static import static
# --- /para trabalhar com imagens

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangoteka.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('estante.urls')),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
